const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const serveIndex = require('serve-index');
const router = require('./api');

let app = express();

app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use('/api', router);
app.use(express.static('JavaScriptTraining'));
app.use(serveIndex('JavaScriptTraining'));
app.listen(3000, () => console.log('listening on http://localhost:3000/'));
