const express = require('express');

let router = express.Router();

router.get('/hello', (req, res) => {
  res.json({message: 'express rest says hi'});
});
router.post('/hello', (req, res) => {
  console.log('readable:', req.readable);
  console.log(req.body);
  res.json(req.body);
});

module.exports = router;
