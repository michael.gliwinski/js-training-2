package mypackage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/EchoInUppercaseMultipleTimesServlet")
public class EchoInUppercaseMultipleTimesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String text = request.getParameter("text");
		int repeatCount = Integer.parseInt(request.getParameter("repeatCount"));
		
        String result = "";
        for (int i = 0; i < repeatCount; i++) {
            result += text + " ";
        }
        
		response.getWriter().print(result.toUpperCase());
	}
}
